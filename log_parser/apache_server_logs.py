import re
from collections import Counter
import csv


def reader(file_name):
    reg_exp = r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'

    with open(file_name) as file:
        log = file.read()

    return re.findall(reg_exp, log)


def count(ip_list):
    return Counter(ip_list)


def write_csv(ip_count):
    with open('ips.csv', 'w') as file:
        writer = csv.writer(file)

        header = ('IP', 'Frequency')
        writer.writerow(header)

        for x, y in ip_count.items():
            writer.writerow((x, y))


if __name__ == '__main__':
    ips = reader('test.log')
    ip_amount = count(ips)
    write_csv(ip_amount)
