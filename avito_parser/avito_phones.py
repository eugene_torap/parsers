import requests
from bs4 import BeautifulSoup
import csv


def write_csv(data):
    with open('avito.csv', 'a') as file:
        writer = csv.writer(file)
        writer.writerow((
            data['title'],
            data['price'],
            data['location'],
            data['url']
        ))


def get_page_data(html):
    soup = BeautifulSoup(html, 'lxml')
    ads = soup.find('div', class_='catalog-list').find_all('div', class_='item_table')

    for ad in ads:
        try:
            title = ad.find('h3').text.strip()
        except Exception:
            title = ''

        try:
            url = 'https://www.avito.ru' + ad.find('h3').find('a').get('href')
        except Exception:
            url = ''

        try:
            price = ad.find('div', class_='about').text.strip()
        except Exception:
            price = ''

        try:
            location = ad.find('div', class_='data').find_all('p')[-1].text.strip()
        except Exception:
            location = ''

        data = {
            'title': title,
            'url': url,
            'price': price,
            'location': location
        }

        return data


def main():
    url = 'https://www.avito.ru/rossiya/telefony?p={}&q=iphone'
    with open('avito.csv', 'w') as file:
        writer = csv.writer(file)
        writer.writerow(('Title', 'Price', 'Location', 'URL'))

    for i in range(1, 5):
        html_data = requests.get(url.format(i)).text
        parsed_data = get_page_data(html_data)
        write_csv(parsed_data)


if __name__ == '__main__':
    main()
